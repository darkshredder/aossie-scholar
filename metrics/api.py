from metrics.models import ScholarProfile
from rest_framework import viewsets, permissions
from .serializers import ScholarProfileSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action


# ScholarProfile Viewset


class ScholarProfileViewSet(viewsets.ModelViewSet):
    queryset = ScholarProfile.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = ScholarProfileSerializer

    @action(detail=False, methods=['post'])
    def search_name(self, request):
        event = ScholarProfile.objects.filter(
            author_name__icontains=request.data['search_text'])
        eventSerializer = ScholarProfileSerializer(
            event, many=True)
        final_data = eventSerializer.data
        final_status = status.HTTP_200_OK

        return Response(data=final_data, status=final_status)
